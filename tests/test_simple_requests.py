from collections import namedtuple

import pytest
from click.testing import CliRunner

from dropbox_api_team_6.simple_requests import main


@pytest.mark.parametrize('content', [
    'abc', 'a',
])
def test_print_response(content, mocker):
    fake_response = namedtuple('fake_response', ('text',))
    mocker.patch('requests.get', return_value=fake_response(text=content))

    runner = CliRunner()
    invoke_result = runner.invoke(main, ['print-response', 'https://example.com'])
    assert invoke_result.output.strip() == content
